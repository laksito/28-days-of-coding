import Head from 'next/head'
import { useEffect, useState } from 'react'

type Battery = {
  addEventListener: () => any
}

type GetBattery = () => Promise<Battery>

type Navigator = {
  getBattery: GetBattery
}

export default function Day9 () {
  const [level, setLevel] = useState(100)
  const [isCharging, setIsCharging] = useState(false)

  useEffect(() => {
    const n = navigator as unknown as Navigator

    if (typeof n.getBattery === 'function') {
      n.getBattery()
        .then((battery: any) => {
          battery.addEventListener('levelchange', () => {
            setLevel(battery.level)
          })

          battery.addEventListener('chargingchange', () => {
            setIsCharging(battery.charging)
          })

          setLevel(battery.level)
          setIsCharging(battery.charging)
        })
    }
  }, [])

  return (
    <>
      <Head>
        <title>Day 9</title>
      </Head>
      <main>
        <div>
          {level}
          <progress value={level} max="100"></progress>
          <div>
            {isCharging}
          </div>
        </div>
      </main>
    </>
  )
}
