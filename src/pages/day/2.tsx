import Head from 'next/head'
import styled from '@emotion/styled'
import { useCallback, useEffect, useState } from 'react'
import Canvas from '@/components/Canvas'

const Main = styled.main`
  height: 100vh;
  background: black;
  display: grid;
  place-items: center;
`

export default function Day2 () {
  const [tick, setTick] = useState(0)

  useEffect(() => {
    let id: number

    const animate = () => {
      id = requestAnimationFrame(animate)
      setTick(prev => prev + 1)
    }

    animate()

    return () => {
      cancelAnimationFrame(id)
    }
  }, [])

  useCallback(() => {
  }, [tick])

  return (
    <>
     <Head>
        <title>Day 2 - Spaceship pew pew</title>
      </Head>
      <Main>
        <Canvas tick={tick} />
      </Main>
    </>
  )
}
